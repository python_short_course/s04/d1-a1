from abc import ABC

class Animal(ABC):
    
    def eat(self, food):       
        pass
    
    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age):
       super().__init__()

       self._name = name
       self._breed = breed
       self._age = age

    def get_name(self):
        print(f'My dogs name is {self._name}')
    
    def set_name(self, name):
        self._name = name

    def get_breed(self):
        print(f'My dogs breed is {self._breed}')
    
    def set_breed(self, breed):
        self._breed = breed
    
    def get_age(self):
        print(f'My dogs age is {self._age}')
    
    def set_age(self, age):
        self._age = age

    def call(self):
        print(f'Here! {self._name}')

    def eat(self, food):
        self._food = food
        print(f'Eaten {self._food}')    
    
    def make_sound(self):
        print('Bark! Woof! Woof!')

class Cat():

    def __init__(self, name, breed, age):
        super().__init__()

        self._name = name
        self._breed = breed
        self._age = age


    def get_name(self):
        print(f'My Cats name is {self._name}')
    
    def set_name(self, name):
        self._name = name

    def get_breed(self):
        print(f'My Cats breed is {self._breed}')
    
    def set_breed(self, breed):
        self._breed = breed
    
    def get_age(self):
        print(f'My Cats age is {self._age}')
    
    def set_age(self, age):
        self._age = age
    
    def call(self):
        print(f'{self._name}, come on!')
    
    def eat(self, food):
        self._food = food
        print(f'Serve me {self._food}')
    
    def make_sound(self):
        print(f'Miaow! Nyaw! Nyaaaa!')

    



dog1 = Dog('Isis', 'Dalmatian', 5)
dog1.eat('Steak')
dog1.make_sound()
dog1.call()

cat1 = Cat('Pussy', 'Persian', 2)
cat1.eat('Tuna')
cat1.make_sound()
cat1.call()